import './App.css';
import Ballot from './Components/Ballot/Ballot';

function App() {
  return (
    <div className="App">
      <header className="App-header">AWARDS 2021</header>
      <Ballot />
    </div>
  );
}

export default App;
