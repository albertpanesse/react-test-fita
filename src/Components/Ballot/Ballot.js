import React, { useEffect, useState } from 'react';
import shortHash from 'short-hash';

import BallotCard from '../BallotCard';
import ConfirmDialog from '../ConfirmDialog';
import api from '../../Api/Api';

import './style.css';

const Ballot = function () {
  const [allBallots, setAllBallots] = useState([]);
  const [confirmDialogShown, setConfirmDialogShown] = useState(false);

  useEffect(() => {
    const getBallotData = async () => {
      const ballot = await api.getBallotData();
      
      const _allBallots = ballot.items.map(ballotCat => {
        const newItem = { ...ballotCat };
        const _items = newItem.items && newItem.items.length > 0 ? newItem.items.map(item => {
          return {
            ...item,
            isSelected: false
          };
        }) : null;
        newItem.items = _items;
        
        return newItem;
      });
    
      setAllBallots(_allBallots);
    }

    getBallotData();
  }, []);

  const ballotOnSelect = (catId, itemId) => {
    const _allBallots = allBallots.map(ballotCat => {
      const newItem = { ...ballotCat };
      const _items = newItem.items && newItem.items.length > 0 ? newItem.items.map(item => {
        if (ballotCat.id === catId) {
          return {
            ...item,
            isSelected: item.id === itemId
          };  
        }

        return item;
      }) : null;
      newItem.items = _items;
      
      return newItem;
    });
  
    setAllBallots(_allBallots);  
  };

  const submitOnClick = (e) => {
    e.preventDefault();

    setConfirmDialogShown(true);
  };

  return (
    <div className='ballot'>
      {
        allBallots && allBallots.length > 0 ? allBallots.map(ballot => (
          <React.Fragment key={`cat-${shortHash(ballot.title)}`}>
            <div className='ballot-category'>
              <div className='ballot-category-title'>{ballot.title}</div>
            </div>
            <div className='ballot-items'>
              {
                ballot.items && ballot.items.length > 0 ? ballot.items.map(item => <BallotCard item={item} onSelect={(id) => ballotOnSelect(ballot.id, id)} key={`item-${shortHash(item.title)}`} />) : null
              }
            </div>
          </React.Fragment>
        )) : null
      }
      <button type="button" className='btn-submit' onClick={submitOnClick}>Submit Ballot</button>

      {confirmDialogShown && <ConfirmDialog isShown={confirmDialogShown} onExit={() => setConfirmDialogShown(false)} />}
    </div>
  )
};

export default Ballot;