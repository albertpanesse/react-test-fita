import React from 'react';

import './style.css';

const BallotCard = function ({ item, onSelect }) {
  return (
    <div className={`ballot-item ${item.isSelected ? 'selected' : ''}`}>
      <div className='ballot-item-title'>{item.title}</div>
      <div className='ballot-item-img'><img src={item.photoUrL} alt={item.title} /></div>
      <div className='ballot-item-actions'><button type='button' className='btn-select' onClick={() => onSelect(item.id)}>Select</button></div>
    </div>
  );
};

export default BallotCard;
