import React from 'react';

import Overlay from '../Overlay';

import './style.css';

const ConfirmDialog = function ({
  isShown,
  onExit,
}) {
  return (
    <Overlay isShown={isShown} onExit={onExit} exitButtonOnly>
      <div className='confirm-dialog'>
        <h1>Success</h1>
      </div>
    </Overlay>
  );
};


export default ConfirmDialog;
