import React from 'react';

import timesDark from '../../images/times-dark.png';
import './style.css';

const Overlay = function ({
  isShown,
  children,
  onExit,
  yOffset,
  exitButtonOnly,
}) {
  return (
    <>
      <div className={`overlay-bg ${isShown ? 'shown' : ''}`} onClick={!exitButtonOnly ? onExit : null} />
      <div className={`overlay-cnt ${isShown ? 'shown' : ''}`} style={{top: `calc(50% + ${yOffset}px)`}}>        
        <div className="exit-btn" onClick={onExit}><img src={timesDark} alt="exit" /></div>
        {children}
      </div>
    </>
);
};

export default Overlay;
